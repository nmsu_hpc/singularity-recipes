SHELL := /bin/bash
.PHONY: all PYTHON3 PIP3 $(MAKECMDGOALS)

default: help

## Create Python VENV + Setup Git Hooks
bootstrap:
	@.scripts/bootstrap.sh

## Recreate Python VENV + Setup Git Hooks
bootstrap-refresh:
	@.scripts/bootstrap.sh refresh

## Cleanup Merged+Deleted Branches
git-cleanup:
	@.scripts/git.sh cleanup

## Commit Changes (Using Python VENV For Pre-Commit Hooks)
git-commit: bootstrap
	@.scripts/git.sh commit

## Push New Branch to Remote
git-push-new:
	@.scripts/git.sh pushnew

## Show Git User Info (name/email)
git-user-list:
	@.scripts/git.sh userlist

## Set Git User Info (name/email)
git-user-set:
	@.scripts/git.sh userset

## Print Source Command for Python VENV
print-venv:
	@echo "source .cache/python-venv/bin/activate"

## Lint Ansible yml/yaml files (ansible-lint)
lint: bootstrap
	@.scripts/lint.sh

# See <https://gist.github.com/klmr/575726c7e05d8780505a> for explanation.
.PHONY: show-help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=19 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'|more $(shell test $(shell uname) == Darwin && echo '-Xr')
