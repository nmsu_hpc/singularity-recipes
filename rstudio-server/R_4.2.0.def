BootStrap: localimage
From: build/R_Base.sif

%environment
export PATH=/usr/lib/rstudio-server/bin:${PATH}

%post
# Setup: Control Variables
TZ="America/Denver"
DISTRO="el8"
R_VERSION="4.2.0"
R_SVER="$(echo $R_VERSION | cut -d '.' -f 1,2 )"
RSTUDIO_VERSION="2022.02.2-485"

# R: Install R (https://docs.rstudio.com/resources/install-r/)
curl -O https://cdn.rstudio.com/r/centos-8/pkgs/R-${R_VERSION}-1-1.x86_64.rpm
dnf install -y R-${R_VERSION}-1-1.x86_64.rpm
rm -f R-${R_VERSION}-1-1.x86_64.rpm
ln -s /opt/R/${R_VERSION}/bin/R /usr/local/bin/R
ln -s /opt/R/${R_VERSION}/bin/Rscript /usr/local/bin/Rscript

# R: Site Profile - Add a default CRAN mirror
echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl')" >> /opt/R/${R_VERSION}/lib/R/etc/Rprofile.site

# R: Site ENV - Add Timezone
echo "TZ='$TZ'" >> /opt/R/${R_VERSION}/lib/R/etc/Renviron

# R: Site ENV - Adjust Platform and Libs
sed -i "/^R_PLATFORM=/ c\R_PLATFORM=\${R_PLATFORM-'$DISTRO-x86_64-singularity'}" /opt/R/${R_VERSION}/lib/R/etc/Renviron
sed -i "/^R_LIBS_USER=/ c\R_LIBS_USER=\${R_LIBS_USER-'~/R/$DISTRO-x86_64-singularity-library/$R_SVER'}" /opt/R/${R_VERSION}/lib/R/etc/Renviron

# R: Site ENV - Add Aditional Custom Vars
echo "DOWNLOAD_STATIC_LIBV8='1'" >> /opt/R/${R_VERSION}/lib/R/etc/Renviron

# R: Install Additional R Packages
R -e "install.packages(c('devtools', 'tidyverse', 'V8', 'rmarkdown', 'distill'), dependencies = TRUE)"

# RStudio: Install R Studio Server
curl -O https://download2.rstudio.org/server/rhel8/x86_64/rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm
dnf install -y rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm
rm -f rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm

%test
command -v R &>/dev/null || exit 1
command -v Rscript &>/dev/null || exit 2
command -v rstudio-server &>/dev/null || exit 3
command -v rserver &>/dev/null || exit 4
