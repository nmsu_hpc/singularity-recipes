#!/bin/bash

#SBATCH --job-name r_test2
#SBATCH --output slurm.%x.j%j.out # slurm.{job_name}.j{job_id}.out
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=2G
#SBATCH --gpus-per-task=1
#SBATCH --partition=interactive
#SBATCH --time=0-3:01:00

# Set Singularity Container File
sfile="${HOME}/singularity/R_4.1.2.sif"

# Setup Execution String For Ease Of Use
sexec=("singularity" "exec" "--nv" "--bind=$TMPDIR:/tmp" "$sfile")

# Run R Programs
srun --ntasks=1 --exclusive "${sexec[@]}" Rscript program.R "1" &
srun --ntasks=1 --exclusive "${sexec[@]}" Rscript program.R "2" &
wait
