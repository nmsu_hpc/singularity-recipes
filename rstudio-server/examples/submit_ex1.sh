#!/bin/bash

#SBATCH --job-name r_test1
#SBATCH --output slurm.%x.j%j.out # slurm.{job_name}.j{job_id}.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=2G
#SBATCH --partition=interactive
#SBATCH --time=0-3:01:00

# Set Singularity Container File
sfile="${HOME}/singularity/R_4.1.2.sif"

# Setup Singularity Execution String For Ease Of Use
sexec=("singularity" "exec" "$sfile")

# Run R Programs
"${sexec[@]}" Rscript program.R "1"
