BootStrap: docker
From: docker.io/rockylinux/rockylinux:8

%runscript
exec "$@"

%environment
export TZ="America/Denver"
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export CPATH=$CPATH:/usr/include/openmpi-x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

%post
# Setup Environment and Fix Locale
dnf install -y langpacks-en glibc-langpack-en glibc-locale-source glibc-common
localedef --quiet -v -c -i en_US -f UTF-8 en_US.UTF-8 || if [ $? -ne 1 ]; then exit $?; fi
export TZ="America/Denver"
export CPATH=$CPATH:/usr/include/openmpi-x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

# Install Additional Repos
dnf install -y epel-release dnf-plugins-core
dnf config-manager --set-enabled powertools

# Update Packages
dnf upgrade -y

# Install Required Dev Tools
dnf groupinstall -y --with-optional "Development Tools" "Scientific Support"
dnf install -y wget ca-certificates mariadb-connector-c-devel curl-devel openssl-devel llvm llvm-devel llvm-static llvm-toolset libxml2-devel
echo "/usr/lib64/openmpi/lib" > /etc/ld.so.conf.d/openmpi.conf
ldconfig

# Install Additional Common Packages Used By R Packages
## https://clint.id.au/?p=1428
dnf module enable -y nodejs:16
dnf install -y wget nodejs nodejs-devel npm openblas java-1.8.0-openjdk-devel zlib-devel libicu-devel libpng-devel libcurl-devel libxml2-devel openssl-devel openmpi-devel python3-numpy python3-matplotlib netcdf4-python3 netcdf-devel netcdf python3-pandas python3-basemap proj-devel gdal-devel monitorix gnuplot ImageMagick librsvg2-devel libsodium-devel libwebp-devel cairo-devel hunspell-devel openssl-devel poppler-cpp-devel protobuf-devel mariadb-devel redland-devel cyrus-sasl-devel libtiff-devel tcl-devel tk-devel xauth mesa-libGLU-devel glpk-devel libXt-devel gsl-devel fftw-devel bzip2-devel geos-devel gtk2-devel gtk3-devel libjpeg-turbo-devel blas-devel lapack-devel mpfr-devel unixODBC-devel libsndfile-devel udunits2-devel postgresql-devel libRmath-devel qt5-devel libdb-devel octave-devel hiredis-devel poppler-glib-devel boost-devel czmq-devel ImageMagick-c++-devel file-devel opencl-headers sqlite-devel

# Cleanup dnf Packages
dnf clean all
rm -rf /var/cache/dnf/*

# Create empty file for nvidia-smi (fixes underlay warning at runtime)
function placeholder(){
	cat <<-EOF > "$1"
		#!/bin/bash
		echo "$1 placeholder for underlay"
	EOF
	chmod +rx "$1"
}
placeholder /usr/bin/nvidia-smi
placeholder /usr/bin/nvidia-persistenced
placeholder /usr/bin/nvidia-debugdump
placeholder /usr/bin/nvidia-cuda-mps-server
placeholder /usr/bin/nvidia-cuda-mps-control
