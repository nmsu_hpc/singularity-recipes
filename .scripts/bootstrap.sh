#!/bin/bash

#set -x
set -e
set -o pipefail

# Verify Working Directory
readlinkf(){ perl -MCwd -e 'print Cwd::abs_path shift' "$1";}
Script_Dir="$(dirname "$(readlinkf "$0")")"
Project_Dir="$(dirname "$Script_Dir")"
if ! [ "$PWD" == "$Project_Dir" ]; then
	echo 'The working directory must be the base of the project!'
	echo 'Preferrable use make and do not call this script directly!!'
	exit 1
fi

# Set Python Virtual Environment Paht
pvenv_dir="$Project_Dir/.cache/python-venv"

# Handle Refresh vs Non-Refresh Scenario
if [ -d "$pvenv_dir" ] && [ "$1" != "refresh" ]; then
	echo "Validating Python Environment"

	# Check For Missing Python Packages
	source "$pvenv_dir/bin/activate"
	if python -m pip freeze -r "$Project_Dir/requirements.txt" |& grep -q "is not installed"; then
		python -m pip install -r "$Project_Dir/requirements.txt"
	fi

	# Ensure Pre-Commit is Installed
	pre-commit install -f | { grep -v "pre-commit installed at .git/hooks/pre-commit" || true; }

	exit 0
fi

# Verify Python 3.8 or Greater Exists
if command -v python3.9 > /dev/null; then
	pycmd=python3.9
elif command -v python3.8 > /dev/null; then
	pycmd=python3.8
elif command -v python3 && python3 -c 'import sys; assert sys.version_info >= (3,8)' > /dev/null; then
	pycmd=python3
elif command -v python && python -c 'import sys; assert sys.version_info >= (3,8)' > /dev/null; then
	pycmd=python
else
	echo 'A version of python of 3.8 or higher was not detected! Install and try again!!'
fi

# Remove Current Environment
if [ -d "$pvenv_dir" ]; then
	if command -v deactivate; then
		deactivate
	fi

	rm -rf "$pvenv_dir"
fi

# Create Environment
$pycmd -m venv "$pvenv_dir"

# Source Environment
source "$pvenv_dir/bin/activate"

# Install Pip & Wheel
$pycmd -m pip install --upgrade pip
$pycmd -m pip install --upgrade wheel

# Insall Requirements File
$pycmd -m pip install -r "$Project_Dir/requirements.txt"

# Ensure Pre-Commit is Installed
pre-commit install -f
