#!/bin/bash

# Verify Working Directory
readlinkf(){ perl -MCwd -e 'print Cwd::abs_path shift' "$1";}
Script_Dir="$(dirname "$(readlinkf "$0")")"
Project_Dir="$(dirname "$Script_Dir")"
if ! [ "$PWD" == "$Project_Dir" ]; then
	echo "The working directory must be the base of the project!"
	echo "Preferrable use make and don't call this script directly!!"
	exit 1
fi

# Tasks defined in arg1/$1
if [ "$1" == "cleanup" ]; then
	mbranch="$(git remote show origin | awk '/HEAD branch/ {print $NF}')"

	git checkout "$mbranch"
	git fetch --prune
	git pull

	# Remove Merged Branches
	git branch --merged | egrep -v "(^\*|${mbranch})" | xargs -I % git branch -d %

	# Remove Squashed+Merged Branches
	git for-each-ref refs/heads/ "--format=%(refname:short)" |
	while read branch; do
		ancestor=$(git merge-base $mbranch $branch) &&
		[[ $(git cherry $mbranch $(git commit-tree $(git rev-parse $branch^{tree}) -p $ancestor -m _)) == "-"* ]] &&
		git branch -D $branch
	done
	if [ $? -eq 1 ]; then exit 0; fi

elif [ "$1" == "pushnew" ]; then
	git push --set-upstream origin "$(git branch  --no-color  | grep -E '^\*' | awk '{print $2}')"

elif [ "$1" == "commit" ]; then
	# Source Environment
	source "$Project_Dir/.cache/python-venv/bin/activate"
	read -rp "Enter Commit Message: " message
	git commit -m "$message"

elif [ "$1" == "userlist" ]; then
	echo
	echo
	echo "Please review your git user configuration!"
	echo
	echo "User (Global)=$(git config --global user.name)"
	echo "Email (Global)=$(git config --global user.email)"
	echo
	echo "User (Local)=$(git config --local user.name)"
	echo "Email (Local)=$(git config --local user.email)"

elif [ "$1" == "userset" ]; then
	read -rp "Git User: " name
	read -rp "Git Email: " email
	echo

	flag=""
	prompt="Please select a configuration realm:"
	PS3="$prompt "
	select opt in local global; do
		if (( REPLY > 0 && REPLY <= 2 )) ; then
			flag="--$opt"
			break
		else
			echo "Invalid option. Try another one." >&2
		fi
	done

	git config $flag user.name "$name"
	git config $flag user.email "$email"

	echo "Git user information for the local repo is set."
	echo "User ($flag)=$(git config $flag user.name)"
	echo "Email ($flag)=$(git config $flag user.email)"

fi
