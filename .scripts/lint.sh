#!/bin/bash

# Verify Working Directory
readlinkf(){ perl -MCwd -e 'print Cwd::abs_path shift' "$1";}
Script_Dir="$(dirname "$(readlinkf "$0")")"
Project_Dir="$(dirname "$Script_Dir")"
if ! [ "$PWD" == "$Project_Dir" ]; then
	echo 'The working directory must be the base of the project!'
	echo 'Preferrable use make and do not call this script directly!!'
	exit 1
fi

# Source Environment
source "$Project_Dir/.cache/python-venv/bin/activate"

# Lint Project with pre-commit
pre-commit run --all-files
