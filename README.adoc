= singularity-recipes

Collection of Singularity Build Files, Notes, and Slurm Integration.

* link:ndnSIM/README.adoc[ndnSIM]

== Contribution Instructions

In order to contribute to this repository do the following:

. Clone repository
. Create a custom branch (feature/name or fix/name)
. Run `make bootstrap` to setup required libraries
. Make changes
. Test
. Push your new branch to gitlab
. Run `make git-commit` instead of `git commit` so that that the required libraries for pre-commit are available.
