= ndnSIM

This container includes required libraries to compile ndnSIM/ns-3 aswell as a few additional libraries as requested by users.

As the workflow to use ndnSIM requires semi frequent recompilation this container does not directly contain ndnSIM.  Use this container to build and run ndnSIM from a bind directory (or User Home directory).

For reference refer to the 2 included Slurm submission scripts.
