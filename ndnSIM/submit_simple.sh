#!/bin/bash

#SBATCH --job-name ndnsim_simple
#SBATCH --output slurm.%x.j%j.out # slurm.{job_name}.j{job_id}.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=2G
#SBATCH --partition=interactive
#SBATCH --time=0-3:01:00

# Set directory variables (logs/ns-3)
ns3_dir="/scratch/${USER}/ndnsim/ndnExample/test_submit_full/NDN_QoS/ns-3"

# Set Singularity Container File
sfile="/scratch/${USER}/ndnsim/ndnSIM.sif"

# Set Extra Bind Mounts to Match Shared Filesystem Mount for scratch
export SINGULARITY_BIND="/scratch:/scratch"

# Move to ns-3 Directory
cd "$ns3_dir" || exit

# Configure and Run ns-3
"$sfile" ./waf configure -d optimized --enable-examples
"$sfile" ./waf
"$sfile" ./waf --run=ndn-case1
