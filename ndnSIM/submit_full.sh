#!/bin/bash

#SBATCH --job-name ndnsim_full
#SBATCH --output slurm.%x.j%j.out # slurm.{job_name}.j{job_id}.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=2G
#SBATCH --partition=interactive
#SBATCH --time=0-3:01:00

# Set directory variables (logs/ns-3)
ns3_dir="/scratch/${USER}/ndnsim/ndnExample/test_submit_full/NDN_QoS/ns-3"

# Set Singularity Container File
sfile="/scratch/${USER}/ndnsim/ndnSIM.sif"

# Set Extra Bind Mounts To Match Shared Filesystem Mount for scratch
bstring="/scratch:/scratch"

# Set srun + singularity prefix for running waf
scommand="srun singularity exec --bind='$bstring' '$sfile'"

# move to ns-3 directory
cd "$ns3_dir" || exit

# ns-3: waf configure
echo -e "#--#\nReport: Starting Waf Configure\n#--#"
"$scommand" ./waf configure -d optimized --enable-examples

# ns-3: waf compile
echo -e "#--#\nReport: Starting Waf Compile\n#--#"
"$scommand" ./waf

# ns-3: waf run
echo -e "#--#\nReport: Starting Waf Run\n#--#"
"$scommand" ./waf --run=ndn-case1
